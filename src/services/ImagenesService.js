import axios from 'axios';

export default class ImagenesService {
    obtenerImagenes(){
       return axios.get('https://picsum.photos/v2/list?page=2&limit=10')
    }
}